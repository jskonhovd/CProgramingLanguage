#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void)
{
    printf("This is a CHAR_BIT: %d\n", CHAR_BIT);
    printf("This is a CHAR_MAX: %d\n", CHAR_MAX);
    printf("This is a CHAR_MIN: %d\n", CHAR_MIN);
    printf("This is a INT_MAX: %d \n", INT_MAX);
    printf("This is a INT_MIN: %d \n", INT_MIN);
    printf("This is a LONG_MAX: %ld \n", LONG_MAX);
    printf("This is a LONG_MIN: %ld \n", LONG_MIN);
    printf("This is a SCHAR_MAX: %d \n", SCHAR_MAX);
    printf("This is a SCHAR_MIN: %d \n", SCHAR_MIN);
    printf("This is a SHRT_MAX: %hd \n", SHRT_MAX);
    printf("This is a SHRT_MIN: %hd \n", SHRT_MIN);
    printf("This is a UCHAR_MAX: %u \n", UCHAR_MAX);
    printf("This is a UINT_MAX: %u \n", UINT_MAX);
    printf("This is a ULONG_MAX: %lu \n", ULONG_MAX);
    printf("This is a USHRT_MAX: %u \n", USHRT_MAX);
    return 0;
}
