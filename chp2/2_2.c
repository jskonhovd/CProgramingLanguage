#define VTAB '\xb'

#include <stdio.h>
#include <limits.h>
#include <float.h>

int getline2(char s[], int lim)
{
  int c, i;
  
  for(i = 0; i < lim-1; i++)
  {
    c = getchar();
    if(c != '\n')
    {
      if(c != EOF)
      {
        s[i] = c;
      }
    }
  }
  return i;
}

int main(void)
{
  int lim = 5;
  char s[lim];
  int len = 0;
  len = getline2(s,lim);
  printf("%s\n",s);
}

