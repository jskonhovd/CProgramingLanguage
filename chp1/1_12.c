#include <stdio.h>
#define IN 1
#define OUT 0

/* Counts lines and Words and stuff " */
int main(void)
{
  int c, state;

  state = OUT;

  while ((c = getchar()) != EOF) {

  if(c == ' ' || c == '\n' || c == '\t')
  {
    if(state == IN)
    {
      state = OUT;
      putchar('\n');
    }
  }
  else
  {
    state = OUT;
    putchar(c);
  }
  }
  return 0;
}
