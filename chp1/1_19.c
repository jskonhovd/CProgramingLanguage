#include <stdio.h>
#define MINLINE 80
#define MAXLINE 1000
/* Solution to 1.19 */

int getline2(char line[], int maxline);
void copy(char to[], char from[]);
void reverse(char to[], char from[], int size);

int main()
{
  int len;
  int max;
  char line[MAXLINE];
  char rev[MAXLINE];

  while((len = getline2(line, MAXLINE)) > 0)
  {  
    reverse(rev, line, len);
    printf("%s", rev);
  }
  return 0;
}

int getline2(char s[], int lim)
{
  int c, i, j;
  for(i = 0, j = 0; (c = getchar())!=EOF && c != '\n'; ++i)
  {
    if(i < lim -1)
    {
      s[j++] = c;
    }
  }

  if (c == '\n') {
    if(i < lim -1)
    {  
      s[j++] = c;
    }
    ++i;
    
  }
  s[j] = '\0';
  return i;
}

void copy(char to[], char from[])
{
  int i;

  i = 0;

  while ((to[i] = from[i]) != '\0')
    ++i;

}

void reverse(char to[], char from[], int len)
{
  int i;

  i = 0;
  for(i = 0; i < len; i++)
  {
    to[i] = from[len-1-i];
  }
  to[i] = '\0';
}
