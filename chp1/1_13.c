/* This is my solution to 1-13 */
#include <stdio.h>
#define MAXWORDLEN 10
#define IN 1
#define OUT 0
int main(void)
{
  int arrl[MAXWORDLEN + 1];
  int wc, i, j, k;
  int state = OUT; 
  int c;
  wc = 0;

  for(j =0; j<MAXWORDLEN; j++)
  {
    arrl[j] = 0;
  }

  while ((c = getchar()) != EOF)
  {
     if(c == ' ' || c == '\n' || c == '\t')
     {
        if(state == OUT)
        {
          state = IN;
          if(wc < MAXWORDLEN)
          {
              arrl[wc]++;
              wc = 0;
          }
          else
          {
              arrl[MAXWORDLEN]++;
              wc = 0;
          }
        }
     }
     else
     {
        state = OUT;
        wc++;
     }
  }

  for(i = 1; i < MAXWORDLEN; i++)
  {
    printf("%d:", i);
    for(k = 0; k < arrl[i]; k++)
    {
      printf("*");
    }
    printf("\n");
  }
  return 0;
}


