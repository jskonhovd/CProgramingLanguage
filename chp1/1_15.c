#include <stdio.h>

float convertFC(float fahr);

int main(void)
{
    float fahr, celsius;
    int lower, upper, step;
    
    lower = 0;
    upper = 300;
    step = 20;


    fahr = lower;
    printf("Fahrenheit - Celsius\n");
    while(fahr <= upper) {
        celsius = convertFC(fahr);
        printf("%3.0f         %6.1f\n", fahr , celsius);
        fahr = fahr + step;
    }
    return 0;
}

float convertFC(float fahr)
{
  return (5.0/9.0) * (fahr - 32.0);
}
