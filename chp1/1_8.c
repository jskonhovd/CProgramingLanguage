#include <stdio.h>

int main()
{
  int c, blanks, tabs, newlines;
  blanks = 0;
  tabs = 0;
  newlines = 0;

  while((c=getchar()) != EOF)
  {
     if (c == ' ') 
     {
        blanks++;
     }
     if( c == '\n')
     {
        tabs++;
     }
        
     if ( c == '\t')
     {
        newlines++;
     }
     
  } 
  printf("Blanks: %d\nTabs: %d\nNewLines: %d\n", blanks, tabs, newlines);
  return 0;
}
