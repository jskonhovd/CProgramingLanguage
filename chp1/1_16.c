#include <stdio.h>
#define MAXLINE 10

int getline2(char line[], int maxline);
void copy(char to[], char from[]);

int main()
{
  int len;
  int max;
  int prevmax;
  int getmore;

  char line[MAXLINE];
  char longest[MAXLINE];
  char temp[MAXLINE];

  max = prevmax = getmore = 0;
  while((len = getline2(line, MAXLINE)) > 0)
  {  
    if(line[len -1 ] != '\n')
    {
          prevmax += len;
          if(max < prevmax)
          {
            max = prevmax;
          }
          if(getmore == 0)
          {
            copy(temp, line);
          }
          getmore = 1;
    }
    else
    {
      if(getmore == 1)
      {
          if(max < prevmax + len)
          {
              max = prevmax + len;
              copy(longest, temp);
              longest[MAXLINE -2] = '\n';
          }
          getmore = 0;
      }
      else if(len > max) {
        max = len;
        copy(longest, line);
      }
      prevmax = 0;
  }
  }
  if(max > 0)
  {
    printf("%s", longest);
    printf("len = %d\n", max);
  }
  return 0;
}

int getline2(char s[], int lim)
{
  int c, i;

  for(i =0; i<lim-1 && (c=getchar())!= EOF && c!='\n'; ++i)
    s[i] = c;

  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  else if(c == EOF && i > 0)
  {
    s[i] = '\n';
    ++i;
  }
  s[i] = '\0';
  return i;
}

void copy(char to[], char from[])
{
  int i;
  i = 0;
  while ((to[i] = from[i]) != '\0')
  { 
    ++i;
  }
}
