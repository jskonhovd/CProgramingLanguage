#include <stdio.h>
#define MAXLINE 1000
/*Solution 1.20- Yes, N should be a variable. */



int gtline(char line[], int maxline);

int main(void)
{
    int n;
    int len;
    int i;
    int x;
    n = 4;
    x = 0;
    char line[MAXLINE];
    while ((len = gtline(line, MAXLINE)) > 0)
    {
        
        for(i = 0; i < len; i++)
        {
           if(line[i] == '\t')
           {
                for(x = 0; x < n; x++)
                {
                    putchar(' ');
                }
           }
           else
           {
                putchar(line[i]);
           }
        }
    }
    return 0;
}

int gtline(char s[], int lim)
{
    int c, i;

    for(i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
        s[i] = c;
    if ( c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}


