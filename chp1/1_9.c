#include <stdio.h>

int main(void)
{
  int c;
  int foo;

  while((c = getchar()) != EOF)
  {
    if(c == ' ')
    {
      if(foo == 0)
      {
        foo = 1;
        putchar(c);
      }
    }
    if(c != ' ')
    {
      foo = 0;
      putchar(c);
    }
  }
  return 0;
}
