/* This is my solution to 1-14 */
#include <stdio.h>
#define MAXWORDLEN 256
#define IN 1
#define OUT 0
int main(void)
{
  int arrl[MAXWORDLEN + 1];
  int wc, i, j, k;
  int state = OUT; 
  int c;
  wc = 0;

  for(j =0; j<MAXWORDLEN; j++)
  {
    arrl[j] = 0;
  }

  while ((c = getchar()) != EOF)
  {
          if(c < MAXWORDLEN)
          {
              arrl[c]++;
          }
          else
          {
              arrl[MAXWORDLEN]++;
          }
  }
  printf("\n");
  for(i = 1; i < MAXWORDLEN; i++)
  {
    printf("%d:", i);
    for(k = 0; k < arrl[i]; k++)
    {
      printf("*");
    }
    printf("\n");
  }
  return 0;
}


