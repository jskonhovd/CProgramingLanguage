#include <stdio.h>
/*This program is my solution for exercise 1-10 */
int main(void)
{
  int c;
  int foo;

  while((c = getchar()) != EOF)
  {
    foo = 0;
    if(c == '\t')
    {
      putchar('\\');
      putchar('t');
      foo = 1;
    }
    if(c == '\\')
    {
      putchar('\\');
      putchar('\\');
      foo = 1;
    }
    if(c == '\b')
    {
      putchar('\\');
      putchar('b');
      foo = 1;
    }
    if(foo == 0)
    {
      putchar(c);
    }
  }
  return 0;
}
